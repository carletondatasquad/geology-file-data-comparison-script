# Geology File Data Comparison Script
# Data Squad
# DS member: Ozzy Houck
# Takes in two directories and returns the files that are missing from one of
# them but are in the originalself

import os  # Package used to search through files


# searches through the directory and makes list of file objects
def search(path):
    walk_dir = os.path.abspath(path)
    ls = []
    for root, subdirs, files in os.walk(walk_dir):
        for name in files:
            ls.append(name)
    return ls


# function to get file paths for all the missing files
def searchpath(missingFiles, path):
    walk_dir = os.path.abspath(path)
    ls = []
    for root, subdirs, files in os.walk(walk_dir):
        for name in files:
            if name in missingFiles:
                filepath = os.path.join(root, name)
                ls.append(filepath)
    return ls


# Function that returns the difference between two lists
def Diff(x, y):
    return (list(set(y) - set(x)))
   # return [item for item in y if item not in x]


def main():
    allFiles = search("G:")  # Connect to the network drive
    print("Length of all files")
    print(len(allFiles))  # How many files are in the network drive?
    sentFiles = search("./dropbox")  # Connect to the dropbox folder
    print("Length of sent files")
    print(len(sentFiles))  # How many files are in the dropbox folder
    missingFiles = Diff(sentFiles, allFiles)  # Creates a set of the files that didn't tranfer
    print("Length of missing files")
    print(len(missingFiles))  # How many didn't transfer?
   # print(missingFiles)
    filepaths = searchpath(missingFiles, "G:")
    print("Length of filepaths")
    print(len(filepaths))
   # print(filepaths)

main()
